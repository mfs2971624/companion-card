import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CompanionCardSharedModule } from 'app/shared/shared.module';
import { ACTIVATE_CARD_ROUTE } from './activate-card.route';
import { ActivateCardComponent } from './activate-card.component';

@NgModule({
  imports: [CommonModule, CompanionCardSharedModule, RouterModule.forChild([ACTIVATE_CARD_ROUTE])],
  declarations: [ActivateCardComponent],
})
export class CompanionCardActivateCardModule { }
