export interface IActivateCard {
  cardNumber?: string;
  phoneNumber?: string;
  pinCode?: string;
  lang?: string;
}

export class ActivateCard implements IActivateCard {
  constructor(
    public cardNumber?: string,
    public phoneNumber?: string,
    public pinCode?: string,
    public lang?: string
  ) {}
}
