import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { IActivateCard } from './activate-card.model';

type EntityResponseType = HttpResponse<String>;

@Injectable({ providedIn: 'root' })
export class ActivateCardService {
  public resourceUrl = SERVER_API_URL + 'api/activate-card';

  constructor(protected http: HttpClient) {}

  activateCard(activateCard: IActivateCard): Observable<EntityResponseType> {
    return this.http.post<String>(this.resourceUrl, activateCard, { observe: 'response' });
  }

}
