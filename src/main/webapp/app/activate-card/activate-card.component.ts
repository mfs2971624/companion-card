import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IActivateCard, ActivateCard } from './activate-card.model';
import { ActivateCardService } from './activate-card.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-activate-card',
  templateUrl: './activate-card.component.html',
  styleUrls: ['./activate-card.component.scss']
})
export class ActivateCardComponent implements OnInit {

  activateCard!: ActivateCard;
  isSaving = false;

  editForm = this.fb.group({
    cardNumber: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
    phoneNumber: ['', [Validators.required]],
    pinCode: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
  });

  constructor(
    private activateCardService: ActivateCardService,
    private fb: FormBuilder,
    private eventManager: JhiEventManager
  ) { }

  ngOnInit(): void {
  }

  private initializeForm(): void {
    this.editForm.patchValue({
      cardNumber: '',
      phoneNumber: '',
      pinCode: ''
    });
  }

  save(): void {
    this.isSaving = true;
    const activateCard = this.createFromForm();
    this.subscribeToSaveResponse(this.activateCardService.activateCard(activateCard));
  }

  private createFromForm(): IActivateCard {
    return {
      ...new ActivateCard(),
      cardNumber: this.editForm.get(['cardNumber'])!.value,
      phoneNumber: this.editForm.get(['phoneNumber'])!.value,
      pinCode: this.editForm.get(['pinCode'])!.value,
      lang: '2',
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<String>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.cancel();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  cancel(): void {
    this.initializeForm();
  }
}
