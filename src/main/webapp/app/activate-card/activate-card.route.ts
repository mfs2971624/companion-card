import { Route } from '@angular/router';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

import { ActivateCardComponent } from './activate-card.component';

export const ACTIVATE_CARD_ROUTE: Route = {
  path: '',
  component: ActivateCardComponent,
  data: {
    authorities: [Authority.ADMIN],
    pageTitle: 'Activation Carte',
  },
  canActivate: [UserRouteAccessService],
};
