import { Moment } from 'moment';

export interface ITransaction {
  id?: number;
  transactionId?: string;
  transactionType?: string;
  transactionDate?: Moment;
  transactionAmount?: number;
  transactionData?: string;
  reference?: string;
  responseStatusComviva?: string;
  responseDetailsComviva?: string;
  referenceId?: string;
  operationName?: string;
}

export class Transaction implements ITransaction {
  constructor(
    public id?: number,
    public transactionId?: string,
    public transactionType?: string,
    public transactionDate?: Moment,
    public transactionAmount?: number,
    public transactionData?: string,
    public reference?: string,
    public responseStatusComviva?: string,
    public responseDetailsComviva?: string,
    public referenceId?: string,
    public operationName?: string
  ) {}
}
