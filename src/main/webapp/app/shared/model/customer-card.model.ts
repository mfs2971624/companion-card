import { Moment } from 'moment';

export interface ICustomerCard {
  id?: number;
  customerMsisdn?: string;
  customerReference?: string;
  customerFirstName?: string;
  customerLastName?: string;
  cardNumber?: string;
  trackingNumber?: string;
  validDate?: Moment;
  expiryDate?: Moment;
  cardType?: string;
  cardStatus?: string;
  creationDate?: Moment;
  isSelected?: boolean;
}

export class CustomerCard implements ICustomerCard {
  constructor(
    public id?: number,
    public customerMsisdn?: string,
    public customerReference?: string,
    public customerFirstName?: string,
    public customerLastName?: string,
    public cardNumber?: string,
    public trackingNumber?: string,
    public validDate?: Moment,
    public expiryDate?: Moment,
    public cardType?: string,
    public cardStatus?: string,
    public creationDate?: Moment
  ) {}
}
