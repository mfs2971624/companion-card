import { Moment } from 'moment';

export interface IJournal {
  id?: number;
  trackingNumber?: string;
  reason?: string;
  resultcode?: string;
  message?: string;
  date?: Moment;
  spare1?: string;
  spare2?: string;
  login?: string;
}

export class Journal implements IJournal {
  constructor(
    public id?: number,
    public trackingNumber?: string,
    public reason?: string,
    public resultcode?: string,
    public message?: string,
    public date?: Moment,
    public spare1?: string,
    public spare2?: string,
    public login?: string
  ) {}
}
