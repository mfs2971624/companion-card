import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { errorRoute } from './layouts/error/error.route';
import { navbarRoute } from './layouts/navbar/navbar.route';
import { DEBUG_INFO_ENABLED } from 'app/app.constants';
import { Authority } from 'app/shared/constants/authority.constants';

import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';

const LAYOUT_ROUTES = [navbarRoute, ...errorRoute];

@NgModule({
  imports: [
    RouterModule.forRoot(
      [
        {
          path: 'admin',
          data: {
            authorities: ['ROLE_G_COMPANION_MFS_ADMINS', 'ROLE_G_COMPANION_MFS_READONLY'],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./admin/admin-routing.module').then(m => m.AdminRoutingModule),
        },
        {
          path: 'account',
          loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
        },
        {
          path: 'activate-card',
          data: {
            authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./activate-card/activate-card.module').then(m => m.CompanionCardActivateCardModule),
        },
        {
          path: 'transaction',
          data: {
            authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./transaction/transaction.module').then(m => m.CompanionCardTransactionModule),
        },
        {
          path: 'customer-card',
          data: {
            authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
          },
          canActivate: [UserRouteAccessService],
          loadChildren: () => import('./entities/customer-card/customer-card.module').then(m => m.CompanionCardCustomerCardModule),
        },
        ...LAYOUT_ROUTES,
      ],
      { enableTracing: DEBUG_INFO_ENABLED }
    ),
  ],
  exports: [RouterModule],
})
export class CompanionCardAppRoutingModule {}
