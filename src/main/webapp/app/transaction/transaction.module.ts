import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CompanionCardSharedModule } from 'app/shared/shared.module';
import { TransactionComponent } from './transaction.component';
import { TransactionDetailComponent } from './transaction-detail.component';
import { transactionRoute } from './transaction.route';

@NgModule({
  imports: [CompanionCardSharedModule, RouterModule.forChild(transactionRoute)],
  declarations: [TransactionComponent, TransactionDetailComponent],
})
export class CompanionCardTransactionModule {}
