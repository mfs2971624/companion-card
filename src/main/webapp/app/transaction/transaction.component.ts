import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { ITransaction, Transaction } from 'app/shared/model/transaction.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { TransactionService } from './transaction.service';

@Component({
  selector: 'jhi-transaction',
  templateUrl: './transaction.component.html',
})
export class TransactionComponent implements OnInit, OnDestroy {
  transactions?: ITransaction[];
  transactionsReport?: Transaction[] = [];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  keyword: any;
  transType: any;
  status: any;
  debut: any;
  fin: any;
  hideMo = true;
  btnClicked = false;

  constructor(
    protected transactionService: TransactionService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.transactionService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<ITransaction[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    // eslint-disable-next-line no-console
    console.log('btn');
    // eslint-disable-next-line no-console
    console.log(this.btnClicked);
    this.handleNavigation();
    this.registerChangeInTransactions();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'desc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ITransaction): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInTransactions(): void {
    this.eventSubscriber = this.eventManager.subscribe('transactionListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'desc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: ITransaction[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/transaction'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          // sort: this.predicate + ',' + (this.ascending ? 'desc' : 'desc'),
        },
      });
    }
    this.transactions = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  searchOrLoadAll(page?: number): void {
    if (
      (!this.keyword || this.keyword === '') &&
      (!this.status || this.status === '') &&
      (!this.transType || this.transType === '') &&
      (!this.debut || this.debut === '') &&
      (!this.fin || this.fin === '')
    ) {
      this.loadPage(page);
    } else {
      this.search(page);
    }
  }

  clear(): void {
    this.btnClicked = false;
    this.keyword = '';
    this.transType = '';
    this.status = '';
    this.debut = '';
    this.fin = '';
    this.loadPage();
  }

  search(page?: number, dontNavigate?: boolean): void {
    this.btnClicked = true;
    // eslint-disable-next-line no-console
    console.log('btn');
    // eslint-disable-next-line no-console
    console.log(this.btnClicked);
    const pageToLoad: number = page || this.page || 1;
    this.transactionService
      .search({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        keyword: this.keyword || '',
        transType: this.transType || '',
        status: this.status || '',
        debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
        fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : '',
      })
      .subscribe(
        (res: HttpResponse<ITransaction[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  generateReport(): void {
    if (this.btnClicked) {
      this.transactionService
        .reportBySearch({
          keyword: this.keyword || '',
          transType: this.transType || '',
          status: this.status || '',
          debut: this.debut !== undefined && this.debut !== '' ? new Date(this.debut).toISOString() : '',
          fin: this.fin !== undefined && this.fin !== '' ? new Date(this.fin).toISOString() : '',
        })
        .subscribe(res => {
          if (res.body?.length! > 200000) {
            this.hideMo = false;
          } else {
            this.hideMo = true;
            this.transactionService.generateExcel(res.body!);
          }
        });
    } else {
      this.transactionService.report().subscribe(res => {
        if (res.body?.length! > 200000) {
          this.hideMo = false;
        } else {
          this.hideMo = true;
          this.transactionService.generateExcel(res.body!);
        }
      });
    }
  }
}
