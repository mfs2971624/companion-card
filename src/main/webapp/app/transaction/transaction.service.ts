import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ITransaction, Transaction } from 'app/shared/model/transaction.model';
import { Workbook } from 'exceljs';
import * as fs from 'file-saver';

type EntityResponseType = HttpResponse<ITransaction>;
type EntityArrayResponseType = HttpResponse<ITransaction[]>;

@Injectable({ providedIn: 'root' })
export class TransactionService {
  public resourceUrl = SERVER_API_URL + 'api/transactions';

  constructor(protected http: HttpClient) {}

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransaction>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  report(): Observable<EntityArrayResponseType> {
    const options = createRequestOption();
    return this.http
      .get<ITransaction[]>(this.resourceUrl + '/report', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  reportBySearch(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransaction[]>(this.resourceUrl + '/report/search', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransaction[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(transaction: ITransaction): ITransaction {
    const copy: ITransaction = Object.assign({}, transaction, {
      transactionDate:
        transaction.transactionDate && transaction.transactionDate.isValid() ? transaction.transactionDate.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.transactionDate = res.body.transactionDate ? moment(res.body.transactionDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transaction: ITransaction) => {
        transaction.transactionDate = transaction.transactionDate ? moment(transaction.transactionDate) : undefined;
      });
    }
    return res;
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransaction[]>(this.resourceUrl + '/search', { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  generateExcel(transactions: ITransaction[]): void {
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Rapport');
    const title = 'Transactions Report';
    const header = [
      'TransactionId',
      'TransactionType',
      'TransactionDate',
      'TransactionAmount',
      'Reference',
      'Resp Status',
      'Resp Details',
      'Ref Id',
      'Operation',
    ];

    // add new row
    worksheet.addRow([]);
    worksheet.addRow([]);
    const titleRow = worksheet.addRow([title]);
    titleRow.font = { bold: true };
    // blank row
    worksheet.addRow([]);

    //add row current date
    const subtitleRow = worksheet.addRow(['Date: ' + moment().format('DD-MM-YYYY')]);

    //add header row
    const headerRow = worksheet.addRow(header);

    // cell style: fill and border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'FFFFFF00' },
        bgColor: { argb: 'FF0000FF' },
      };

      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });

    //add data and conditional formatting
    transactions.forEach(t => {
      // eslint-disable-next-line no-console

      const elt = [
        t.transactionId,
        t.transactionType === '00' ? 'Payment marchand' : 'ATM',
        t.transactionDate?.format('DD-MM-YYYY'),
        t.transactionAmount,
        t.reference,
        t.responseStatusComviva,
        t.responseDetailsComviva,
        t.referenceId,
        t.operationName,
      ];
      const eltRow = worksheet.addRow(elt);
      eltRow.eachCell((cell, number) => {
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      });
    });

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Transactions_Report.xlsx');
    });
  }
}
