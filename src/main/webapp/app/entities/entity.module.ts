import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'customer-card',
        loadChildren: () => import('./customer-card/customer-card.module').then(m => m.CompanionCardCustomerCardModule),
      },
      {
        path: 'journal',
        loadChildren: () => import('./journal/journal.module').then(m => m.CompanionCardJournalModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class CompanionCardEntityModule {}
