import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IJournal, Journal } from 'app/shared/model/journal.model';
import { JournalService } from './journal.service';

@Component({
  selector: 'jhi-journal-update',
  templateUrl: './journal-update.component.html',
})
export class JournalUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    cardNumber: [],
    reason: [],
    resultcode: [],
    message: [],
    date: [],
    spare1: [],
    spare2: [],
    login: [],
  });

  constructor(protected journalService: JournalService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ journal }) => {
      if (!journal.id) {
        const today = moment().startOf('day');
        journal.date = today;
      }

      this.updateForm(journal);
    });
  }

  updateForm(journal: IJournal): void {
    this.editForm.patchValue({
      id: journal.id,
      cardNumber: journal.trackingNumber,
      reason: journal.reason,
      resultcode: journal.resultcode,
      message: journal.message,
      date: journal.date ? journal.date.format(DATE_TIME_FORMAT) : null,
      spare1: journal.spare1,
      spare2: journal.spare2,
      login: journal.login,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const journal = this.createFromForm();
    if (journal.id !== undefined) {
      this.subscribeToSaveResponse(this.journalService.update(journal));
    } else {
      this.subscribeToSaveResponse(this.journalService.create(journal));
    }
  }

  private createFromForm(): IJournal {
    return {
      ...new Journal(),
      id: this.editForm.get(['id'])!.value,
      trackingNumber: this.editForm.get(['trackingNumber'])!.value,
      reason: this.editForm.get(['reason'])!.value,
      resultcode: this.editForm.get(['resultcode'])!.value,
      message: this.editForm.get(['message'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      login: this.editForm.get(['login'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJournal>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
