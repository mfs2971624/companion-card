import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap, Router, Data } from '@angular/router';
import { Subscription, combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';

import { ICustomerCard } from 'app/shared/model/customer-card.model';

import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { CustomerCardService } from './customer-card.service';
import { CustomerCardDeleteDialogComponent } from './customer-card-delete-dialog.component';

@Component({
  selector: 'jhi-customer-card',
  templateUrl: './customer-card.component.html',
})
export class CustomerCardComponent implements OnInit, OnDestroy {
  customerCards?: ICustomerCard[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  isChecked = 0;
  checkedList: any;
  isSaving = false;
  isOneButtonChecked = false;
  keyword: null;
  hide1 = true;
  hide2 = true;
  etat = '';
  message = '';

  idCustomers = [];

  constructor(
    protected customerCardService: CustomerCardService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadPage(page?: number, dontNavigate?: boolean): void {
    const pageToLoad: number = page || this.page || 1;

    this.customerCardService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
        keyword: this.keyword,
      })
      .subscribe(
        (res: HttpResponse<ICustomerCard[]>) => this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.etat = localStorage.getItem('etat')!;
    this.message = localStorage.getItem('message')!;
    if (this.etat === 'stop') {
      this.hide2 = false;
      setInterval(() => {
        this.hide2 = true;
        localStorage.removeItem('etat');
      }, 6000);
    }
    if (this.etat === 'active') {
      this.hide1 = false;
      setInterval(() => {
        this.hide1 = true;
        localStorage.removeItem('etat');
      }, 6000);
    }
    this.handleNavigation();
    this.registerChangeInCustomerCards();
    // this.isOneButtonChecked = false;
    this.isOneElementCheked();
  }

  protected handleNavigation(): void {
    combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data: Data, params: ParamMap) => {
      const page = params.get('page');
      const pageNumber = page !== null ? +page : 1;
      const sort = (params.get('sort') ?? data['defaultSort']).split(',');
      const predicate = sort[0];
      const ascending = sort[1] === 'asc';
      if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
        this.predicate = predicate;
        this.ascending = ascending;
        this.loadPage(pageNumber, true);
      }
    }).subscribe();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICustomerCard): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCustomerCards(): void {
    this.eventSubscriber = this.eventManager.subscribe('customerCardListModification', () => this.loadPage());
  }

  delete(customerCard: ICustomerCard): void {
    const modalRef = this.modalService.open(CustomerCardDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.customerCard = customerCard;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected onSuccess(data: ICustomerCard[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/customer-card'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc'),
        },
      });
    }
    this.customerCards = data || [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }

  isOneElementCheked(): boolean {
    this.checkedList = [];
    if (this.customerCards !== null && this.customerCards !== undefined) {
      for (let i = 0; i < this.customerCards.length; i++) {
        if (this.customerCards[i].isSelected) this.checkedList.push(this.customerCards[i].id);
      }
      if (this.checkedList !== null || this.checkedList !== undefined) {
        // eslint-disable-next-line no-console
        console.log('________dans isOneElementCheked@    ', this.checkedList);

        if (this.checkedList.length > 0) this.isOneButtonChecked = true;
        else this.isOneButtonChecked = false;
      }
    }
    return this.isOneButtonChecked;
  }

  getCheckedItemList(status: string): string[] {
    this.checkedList = [];
    if (this.customerCards !== null && this.customerCards !== undefined) {
      for (let i = 0; i < this.customerCards.length; i++) {
        if (this.customerCards[i].isSelected) {
          this.checkedList.push(this.customerCards[i].id);

          this.customerCards[i].cardStatus = status;

          // eslint-disable-next-line no-console
          console.log('________dans getCheckedItemList@  this.customerCards[i].cardStatus! = ', this.customerCards[i].cardStatus!);
          this.subscribeToSaveResponse(this.customerCardService.update(this.customerCards[i]));

          // this.customerCardService.updateCustomer(this.customerCards[i].id!, this.customerCards[i]);
        }
      }
      // this.checkedList = JSON.stringify(this.checkedList);
      // eslint-disable-next-line no-console
      console.log('________dans getCheckedItemList@    ', this.checkedList);
    }
    return this.checkedList;
  }

  suspendre(): void {
    // eslint-disable-next-line no-console
    console.log('________ dans suspendre() ________     ');

    // eslint-disable-next-line no-console
    // console.log("________ VALUE =  ", value);

    let tab = [];

    if (this.customerCards !== null && this.customerCards !== undefined) {
      // eslint-disable-next-line no-console
      // console.log("________ dans suspendre() ________ @    ");

      tab = this.getCheckedItemList('STOPPED');
      // eslint-disable-next-line no-console
      console.log('________ element tab ________ @    ', tab);
    }

    this.router.navigate(['']);
  }

  activer(): void {
    // eslint-disable-next-line no-console
    console.log('________ dans active() ________     ');

    // eslint-disable-next-line no-console
    // console.log("________ VALUE =  ", value);

    let tab = [];

    if (this.customerCards !== null && this.customerCards !== undefined) {
      // eslint-disable-next-line no-console
      // console.log("________ dans activer() ________ @    ");

      tab = this.getCheckedItemList('ACTIVE');
      // eslint-disable-next-line no-console
      console.log('________ element tab ________ @    ', tab);
    }

    this.router.navigate(['']);
  }

  supprimer(): void {
    // eslint-disable-next-line no-console
    console.log('________ dans supprimer() ________     ');

    // eslint-disable-next-line no-console
    //  console.log("________ VALUE =  ", value);

    let tab = [];

    if (this.customerCards !== null && this.customerCards !== undefined) {
      // eslint-disable-next-line no-console
      // console.log("________ dans supprimer() ________ @    ");

      tab = this.getCheckedItemList('RETIRED');
      // eslint-disable-next-line no-console
      console.log('________ element tab ________ @    ', tab);
    }

    this.router.navigate(['']);
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  previousState(): void {
    window.history.back();
  }
  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerCard>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  isSet(): boolean {
    return this.keyword !== null || this.keyword !== '';
  }

  clear(): void {
    this.keyword = null;
    this.search();
  }

  search(): void {
    if (this.isSet()) {
      this.loadPage();
    }
  }
}
