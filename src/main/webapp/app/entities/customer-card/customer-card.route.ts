import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICustomerCard, CustomerCard } from 'app/shared/model/customer-card.model';
import { CustomerCardService } from './customer-card.service';
import { CustomerCardComponent } from './customer-card.component';
import { CustomerCardDetailComponent } from './customer-card-detail.component';
import { CustomerCardUpdateComponent } from './customer-card-update.component';

@Injectable({ providedIn: 'root' })
export class CustomerCardResolve implements Resolve<ICustomerCard> {
  constructor(private service: CustomerCardService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICustomerCard> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((customerCard: HttpResponse<CustomerCard>) => {
          if (customerCard.body) {
            return of(customerCard.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CustomerCard());
  }
}

export const customerCardRoute: Routes = [
  {
    path: '',
    component: CustomerCardComponent,
    data: {
      authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
      defaultSort: 'id,asc',
      pageTitle: 'CustomerCards',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CustomerCardDetailComponent,
    resolve: {
      customerCard: CustomerCardResolve,
    },
    data: {
      authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
      pageTitle: 'CustomerCards',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CustomerCardUpdateComponent,
    resolve: {
      customerCard: CustomerCardResolve,
    },
    data: {
      authorities: [Authority.ADMIN],
      pageTitle: 'CustomerCards',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CustomerCardUpdateComponent,
    resolve: {
      customerCard: CustomerCardResolve,
    },
    data: {
      authorities: [Authority.ADMIN, Authority.ADMIN_READONLY],
      pageTitle: 'CustomerCards',
    },
    canActivate: [UserRouteAccessService],
  },
];
