import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICustomerCard } from 'app/shared/model/customer-card.model';

@Component({
  selector: 'jhi-customer-card-detail',
  templateUrl: './customer-card-detail.component.html',
})
export class CustomerCardDetailComponent implements OnInit {
  customerCard: ICustomerCard | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerCard }) => (this.customerCard = customerCard));
  }

  previousState(): void {
    window.history.back();
  }
}
