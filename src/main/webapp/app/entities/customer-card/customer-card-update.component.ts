import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICustomerCard, CustomerCard } from 'app/shared/model/customer-card.model';
import { CustomerCardService } from './customer-card.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { IJournal, Journal } from 'app/shared/model/journal.model';
import { JournalService } from '../journal/journal.service';
import { Account } from 'app/core/user/account.model';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-customer-card-update',
  templateUrl: './customer-card-update.component.html',
})
export class CustomerCardUpdateComponent implements OnInit {
  public modalRef?: BsModalRef;
  journal?: IJournal = new Journal();
  isSaving = false;
  validDateDp: any;
  customerCard: ICustomerCard = new CustomerCard();
  status = 'STOP';
  btnColor = '';
  isStop = true;
  hideSp = true;
  reasonSelected = '1';
  resultCode = '';
  resultMessage = '';
  user = '';
  showAlert = true;

  etat = '';
  data = {};

  editForm = this.fb.group({
    id: [],
    customerMsisdn: [],
    customerReference: [],
    customerFirstName: [],
    customerLastName: [],
    cardNumber: [],
    validDate: [],
    expiryDate: [],
    cardType: [],
    cardStatus: [],
    creationDate: [],
  });

  constructor(
    protected customerCardService: CustomerCardService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: BsModalService,
    protected journalService: JournalService,
    private accountService: AccountService,
    private router: Router
  ) {
    this.data['1'] = 'Carte Perdue';
    this.data['2'] = 'Carte Volee';
    this.data['3'] = 'Requete en attente';
    this.data['4'] = 'Consolidation de carte';
    this.data['5'] = 'Carte inactive';
    this.data['6'] = 'Limite essai PIN depassee';
    this.data['7'] = 'Risque de fraude';
    this.data['8'] = 'Carte remplacee';
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ customerCard }) => {
      if (!customerCard.id) {
        const today = moment().startOf('day');
        customerCard.expiryDate = today;
        customerCard.creationDate = today;
      }

      this.updateForm(customerCard);
      this.customerCard = customerCard;
      this.status = this.customerCard.cardStatus?.toUpperCase().startsWith('STOP') ? 'ACTIVE' : this.status;
      this.isStop = this.customerCard.cardStatus?.toUpperCase().startsWith('STOP') ? this.isStop : false;
      this.btnColor = this.customerCard.cardStatus?.toUpperCase().startsWith('STOP') ? 'col-2 btn btn-success' : 'col-2 btn btn-danger';
    });
  }

  updateForm(customerCard: ICustomerCard): void {
    this.editForm.patchValue({
      id: customerCard.id,
      customerMsisdn: customerCard.customerMsisdn,
      customerReference: customerCard.customerReference,
      customerFirstName: customerCard.customerFirstName,
      customerLastName: customerCard.customerLastName,
      cardNumber: customerCard.cardNumber,
      validDate: customerCard.validDate,
      expiryDate: customerCard.expiryDate ? customerCard.expiryDate.format(DATE_TIME_FORMAT) : null,
      cardType: customerCard.cardType,
      cardStatus: customerCard.cardStatus,
      creationDate: customerCard.creationDate ? customerCard.creationDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const customerCard = this.createFromForm();
    if (customerCard.id !== undefined) {
      this.subscribeToSaveResponse(this.customerCardService.update(customerCard));
    } else {
      this.subscribeToSaveResponse(this.customerCardService.create(customerCard));
    }
  }

  private createFromForm(): ICustomerCard {
    return {
      ...new CustomerCard(),
      id: this.editForm.get(['id'])!.value,
      customerMsisdn: this.editForm.get(['customerMsisdn'])!.value,
      customerReference: this.editForm.get(['customerReference'])!.value,
      customerFirstName: this.editForm.get(['customerFirstName'])!.value,
      customerLastName: this.editForm.get(['customerLastName'])!.value,
      cardNumber: this.editForm.get(['cardNumber'])!.value,
      validDate: this.editForm.get(['validDate'])!.value,
      expiryDate: this.editForm.get(['expiryDate'])!.value ? moment(this.editForm.get(['expiryDate'])!.value, DATE_TIME_FORMAT) : undefined,
      cardType: this.editForm.get(['cardType'])!.value,
      cardStatus: this.editForm.get(['cardStatus'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value
        ? moment(this.editForm.get(['creationDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustomerCard>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  openModal(template: any): void {
    this.modalRef = this.modalService.show(template);
  }

  saveActive(): void {
    this.hideSp = false;
    localStorage.removeItem('etat');
    localStorage.setItem('etat', 'active');
    localStorage.setItem('message', 'Tracking Number: ' + this.customerCard.trackingNumber + ' débloqué avec succès');
    this.customerCard.cardStatus = 'Active';
    this.customerCardService
      .callUnStoppingCard({
        cardIdentifier: this.customerCard.trackingNumber,
        reference: this.customerCard.customerReference,
        note: 'card found',
        lang: '2',
        reasonId: '1',
      })
      .subscribe(
        (res: any) => {
          if (res.body) {
            this.resultCode = res.body.doc.param[0].value;
            this.resultMessage = res.body.doc.param[1].value;
            this.subscribeToSaveResponse(this.customerCardService.update(this.customerCard));
            this.getAuthentication();
            this.journal!.reason = 'card found';
            this.log();
          } else {
            this.showAlert = false;
          }
        },
        err => {}
      );
  }

  saveStop(): void {
    this.hideSp = false;
    localStorage.removeItem('etat');
    localStorage.setItem('etat', 'stop');
    localStorage.setItem('message', 'Tracking Number: ' + this.customerCard.trackingNumber + ' bloqué avec succès');
    this.customerCard.cardStatus = 'STOPPED';
    this.customerCardService
      .callStoppingCard({
        cardIdentifier: this.customerCard.trackingNumber,
        reference: this.customerCard.customerReference,
        note: this.data[this.reasonSelected],
        lang: '2',
        reasonId: this.reasonSelected,
      })
      .subscribe(
        (res: any) => {
          if (res.body) {
            this.resultCode = res.body.doc.param[0].value;
            this.resultMessage = res.body.doc.param[1].value;
            this.subscribeToSaveResponse(this.customerCardService.update(this.customerCard));
            this.getAuthentication();
            this.journal!.reason = this.data[this.reasonSelected];
            this.log();
          } else {
            this.showAlert = false;
          }
        },
        err => {}
      );
  }

  log(): void {
    this.journal!.trackingNumber = this.customerCard.trackingNumber;
    this.journal!.date = moment();
    this.journal!.message = this.resultMessage;

    this.journal!.resultcode = this.resultCode;
    this.journal!.login = this.user;

    this.subscribeToSaveResponse(this.journalService.create(this.journal!));
  }

  getAuthentication(): void {
    this.accountService.getAuthenticationState().subscribe(account => {
      this.user = account?.email!;
    });
  }
}
