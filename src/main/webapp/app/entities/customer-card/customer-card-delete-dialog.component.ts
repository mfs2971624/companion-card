import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICustomerCard } from 'app/shared/model/customer-card.model';
import { CustomerCardService } from './customer-card.service';

@Component({
  templateUrl: './customer-card-delete-dialog.component.html',
})
export class CustomerCardDeleteDialogComponent {
  customerCard?: ICustomerCard;

  constructor(
    protected customerCardService: CustomerCardService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.customerCardService.delete(id).subscribe(() => {
      this.eventManager.broadcast('customerCardListModification');
      this.activeModal.close();
    });
  }
}
