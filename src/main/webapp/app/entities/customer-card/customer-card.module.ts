import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';

import { CompanionCardSharedModule } from 'app/shared/shared.module';
import { CustomerCardComponent } from './customer-card.component';
import { CustomerCardDetailComponent } from './customer-card-detail.component';
import { CustomerCardUpdateComponent } from './customer-card-update.component';
import { CustomerCardDeleteDialogComponent } from './customer-card-delete-dialog.component';
import { customerCardRoute } from './customer-card.route';

@NgModule({
  imports: [CompanionCardSharedModule, RouterModule.forChild(customerCardRoute), ModalModule.forRoot()],
  declarations: [CustomerCardComponent, CustomerCardDetailComponent, CustomerCardUpdateComponent, CustomerCardDeleteDialogComponent],
  entryComponents: [CustomerCardDeleteDialogComponent],
  providers: [BsModalService],
})
export class CompanionCardCustomerCardModule {}
