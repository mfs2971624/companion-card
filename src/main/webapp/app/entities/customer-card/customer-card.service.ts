import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICustomerCard } from 'app/shared/model/customer-card.model';

type EntityResponseType = HttpResponse<ICustomerCard>;
type EntityArrayResponseType = HttpResponse<ICustomerCard[]>;

@Injectable({ providedIn: 'root' })
export class CustomerCardService {
  public resourceUrl = SERVER_API_URL + 'api/customer-cards';
  public stoppingUrl = SERVER_API_URL + 'api/stopping_card';
  public unStoppingUrl = SERVER_API_URL + 'api/unstopping_card';

  constructor(protected http: HttpClient) {}

  create(customerCard: ICustomerCard): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerCard);
    return this.http
      .post<ICustomerCard>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(customerCard: ICustomerCard): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(customerCard);
    return this.http
      .put<ICustomerCard>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICustomerCard>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustomerCard[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(customerCard: ICustomerCard): ICustomerCard {
    const copy: ICustomerCard = Object.assign({}, customerCard, {
      validDate: customerCard.validDate && customerCard.validDate.isValid() ? customerCard.validDate.format(DATE_FORMAT) : undefined,
      expiryDate: customerCard.expiryDate && customerCard.expiryDate.isValid() ? customerCard.expiryDate.toJSON() : undefined,
      creationDate: customerCard.creationDate && customerCard.creationDate.isValid() ? customerCard.creationDate.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.validDate = res.body.validDate ? moment(res.body.validDate) : undefined;
      res.body.expiryDate = res.body.expiryDate ? moment(res.body.expiryDate) : undefined;
      res.body.creationDate = res.body.creationDate ? moment(res.body.creationDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((customerCard: ICustomerCard) => {
        customerCard.validDate = customerCard.validDate ? moment(customerCard.validDate) : undefined;
        customerCard.expiryDate = customerCard.expiryDate ? moment(customerCard.expiryDate) : undefined;
        customerCard.creationDate = customerCard.creationDate ? moment(customerCard.creationDate) : undefined;
      });
    }
    return res;
  }

  updateCustomer(id: number, customerCard: ICustomerCard): Observable<Object> {
    return this.http.put(`${this.resourceUrl}/${id}`, customerCard);
  }

  callStoppingCard(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.stoppingUrl, { params: options, observe: 'response' });
  }

  callUnStoppingCard(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.unStoppingUrl, { params: options, observe: 'response' });
  }
}
