import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { CompanionCardSharedModule } from 'app/shared/shared.module';
import { CompanionCardCoreModule } from 'app/core/core.module';
import { CompanionCardAppRoutingModule } from './app-routing.module';
import { CompanionCardHomeModule } from './home/home.module';
import { CompanionCardEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    CompanionCardSharedModule,
    CompanionCardCoreModule,
    CompanionCardHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    CompanionCardEntityModule,
    CompanionCardAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class CompanionCardAppModule {}
