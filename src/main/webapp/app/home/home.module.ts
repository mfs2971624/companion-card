import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CompanionCardSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [CompanionCardSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class CompanionCardHomeModule {}
