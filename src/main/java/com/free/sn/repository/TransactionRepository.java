package com.free.sn.repository;

import com.free.sn.domain.Transaction;

import com.free.sn.domain.Transaction_;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data  repository for the Transaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT t FROM Transaction t ")
    Page<Transaction> findAllByFilter(Pageable pageable);

    @Query(value = "SELECT t FROM Transaction t ")
    List<Transaction> findAllByFilter();

    @Query("select r from Transaction r where (r.reference like ?1 or ?1 is null or ?1 = '' )" +
        "AND (r.transactionType = ?2 or ?2 is null or ?2 = '')  AND (r.responseStatusComviva = ?3 or ?3 is null or ?3 = '')" +
        "and (r.transactionDate >= ?4 or ?4 is null or ?4 = '') and (r.transactionDate <= ?5 )  order by r.transactionDate desc")
    Page<Transaction> search(@Param("keyword") String keyword, @Param("transType") String transType,
                             @Param("status") String status,  @Param("debut") Instant debut,
                             @Param("fin") Instant fin, Pageable pageable);

    @Query("select r from Transaction r where (r.reference like ?1 or ?1 is null or ?1 = '' )" +
        "AND (r.transactionType = ?2 or ?2 is null or ?2 = '')  AND (r.responseStatusComviva = ?3 or ?3 is null or ?3 = '')" +
        "and (r.transactionDate >= ?4 or ?4 is null or ?4 = '') and (r.transactionDate <= ?5 )  order by r.transactionDate desc")
    List<Transaction> search(@Param("keyword") String keyword, @Param("transType") String transType,
                             @Param("status") String status,  @Param("debut") Instant debut,
                             @Param("fin") Instant fin);



}
