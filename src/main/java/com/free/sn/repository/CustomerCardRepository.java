package com.free.sn.repository;

import com.free.sn.domain.CustomerCard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CustomerCard entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerCardRepository extends JpaRepository<CustomerCard, Long> {

    @Query(value = "SELECT c FROM CustomerCard c WHERE " +
        "c.customerMsisdn LIKE :keyword " +
        "OR c.customerReference LIKE :keyword " +
        "OR c.customerFirstName  LIKE :keyword " +
        "OR c.customerLastName  LIKE :keyword " +
        "OR c.cardNumber  LIKE :keyword " +
        "OR c.cardType  LIKE :keyword " +
        "OR c.cardStatus  LIKE :keyword order by c.creationDate desc")
    Page<CustomerCard> findAllByFilter(@Param("keyword") String keyword, Pageable pageable);

    @Query(value = "SELECT c FROM CustomerCard c order by c.creationDate desc")
    Page<CustomerCard> findAllOrder(Pageable pageable);
}
