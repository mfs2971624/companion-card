package com.free.sn.repository;

import com.free.sn.domain.Journal;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Spring Data  repository for the Journal entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JournalRepository extends JpaRepository<Journal, Long> {

    @Query(value = "SELECT j FROM Journal j order by j.date desc")
    Page<Journal> findAllOrder(Pageable pageable);
}
