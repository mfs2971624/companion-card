package com.free.sn.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ActivateCardClient {

    private final Logger log = LoggerFactory.getLogger(ActivateCardClient.class);
    @Value("${application.talend.host}")
    private String talendHost;
    @Value("${application.talend.activateCardUrl}")
    private String activateCardEndpoint;
    private String activateCardUrl = this.talendHost + this.activateCardEndpoint;
    private final RestTemplate restTemplate = new RestTemplate();

    public ActivateCardResponse callActivateCardRequest(Map uriVariables) throws JAXBException {

        // Formatting URL
        String formatURL = formatURL(activateCardUrl, uriVariables);

        // Creating Request Headers
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_XML_VALUE);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        // Call Activation Card Service Request
        ResponseEntity<String> rawResponse = restTemplate.exchange(formatURL, HttpMethod.GET, entity, String.class, uriVariables);

        log.info("response = {}", rawResponse.getBody());

        ActivateCardResponse activateCardResponse = mapResponseToObject(rawResponse.getBody());

        log.info("activateCardResponse = {}", activateCardResponse);

        return activateCardResponse;
    }

    private String formatURL(String serviceURL, Map<String, String> uriVariables) {

        String url = serviceURL + "?%s";

        String parametrizedArgs = uriVariables.keySet().stream().map(k ->
            String.format("%s={%s}", k, k)
        ).collect(Collectors.joining("&"));

        return String.format(url, parametrizedArgs);
    }

    private ActivateCardResponse mapResponseToObject(String xmlResponse) throws JAXBException {

        StringReader reader = new StringReader(xmlResponse);

        JAXBContext jaxbContext = JAXBContext.newInstance(ActivateCardResponse.class);

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        ActivateCardResponse activateCardResponse = (ActivateCardResponse) unmarshaller.unmarshal(reader);

        return activateCardResponse;
    }
}
