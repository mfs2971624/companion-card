package com.free.sn.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "doc")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivateCardResponse {

    @XmlElement(name = "param")
    private List<ActivateCardResponseParam> params;

    public List<ActivateCardResponseParam> getParams() {
        return params;
    }

    public void setParams(List<ActivateCardResponseParam> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "ActivateCardResponse{" +
            "params=" + params +
            '}';
    }
}
