package com.free.sn.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "param")
@XmlAccessorType(XmlAccessType.FIELD)
public class ActivateCardResponseParam {

    @XmlAttribute
    private String name;

    @XmlAttribute
    private String value;

    public ActivateCardResponseParam() {
    }

    public ActivateCardResponseParam(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Params{" +
            "name='" + name + '\'' +
            ", value='" + value + '\'' +
            '}';
    }
}
