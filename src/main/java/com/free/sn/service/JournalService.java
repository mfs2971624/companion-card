package com.free.sn.service;

import com.free.sn.domain.Journal;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Journal}.
 */
public interface JournalService {

    /**
     * Save a journal.
     *
     * @param journal the entity to save.
     * @return the persisted entity.
     */
    Journal save(Journal journal);

    /**
     * Get all the journals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Journal> findAll(Pageable pageable);


    /**
     * Get the "id" journal.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Journal> findOne(Long id);

    /**
     * Delete the "id" journal.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
