package com.free.sn.service;

import com.free.sn.domain.CustomerCard;
import com.free.sn.domain.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link CustomerCard}.
 */
public interface TransactionService {

    /**
     * Filter list transactions by keyword
     * @param pageable
     * @return
     */
    Page<Transaction> findAllByFilter(Pageable pageable);
    List<Transaction> findAllByFilter();


    /**
     *
     * @param id
     * @return
     */
    Optional<Transaction> findById(Long id);



    Page<Transaction> search(String keyword, String transType, String status,Instant debut,  Instant fin, Pageable pageable);
    List<Transaction> search(String keyword, String transType, String status,Instant debut,  Instant fin);
}
