package com.free.sn.service;

import com.free.sn.domain.CustomerCard;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import javax.xml.parsers.ParserConfigurationException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

/**
 * Service Interface for managing {@link CustomerCard}.
 */
public interface CustomerCardService {

    /**
     * Save a customerCard.
     *
     * @param customerCard the entity to save.
     * @return the persisted entity.
     */
    CustomerCard save(CustomerCard customerCard);

    /**
     * Get all the customerCards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CustomerCard> findAll(Pageable pageable);


    /**
     * Get the "id" customerCard.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CustomerCard> findOne(Long id);

    /**
     * Delete the "id" customerCard.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    public String callStopingCard(String cardIdentifier, String reference, String reasonId, String note, String lang) throws ParserConfigurationException;

    public String callUnStopingCard(String cardIdentifier, String reference, String note, String lang) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException;

    public ResponseEntity<String> callRetireCard(String cardIdentifier, String reference, String lang) ;

    Page<CustomerCard> findAllByFilter(String keyword, Pageable pageable);

}
