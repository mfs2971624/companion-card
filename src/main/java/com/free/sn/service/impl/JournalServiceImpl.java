package com.free.sn.service.impl;

import com.free.sn.service.JournalService;
import com.free.sn.domain.Journal;
import com.free.sn.repository.JournalRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Journal}.
 */
@Service
@Transactional
public class JournalServiceImpl implements JournalService {

    private final Logger log = LoggerFactory.getLogger(JournalServiceImpl.class);

    private final JournalRepository journalRepository;

    public JournalServiceImpl(JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    @Override
    public Journal save(Journal journal) {
        log.debug("Request to save Journal : {}", journal);
        return journalRepository.save(journal);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Journal> findAll(Pageable pageable) {
        log.debug("Request to get all Journals");
        return journalRepository.findAllOrder(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<Journal> findOne(Long id) {
        log.debug("Request to get Journal : {}", id);
        return journalRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Journal : {}", id);
        journalRepository.deleteById(id);
    }
}
