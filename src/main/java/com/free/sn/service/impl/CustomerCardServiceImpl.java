package com.free.sn.service.impl;

import com.free.sn.service.CustomerCardService;
import com.free.sn.domain.CustomerCard;
import com.free.sn.repository.CustomerCardRepository;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Service Implementation for managing {@link CustomerCard}.
 */
@Service
@Transactional
public class CustomerCardServiceImpl implements CustomerCardService {

    private final Logger log = LoggerFactory.getLogger(CustomerCardServiceImpl.class);

    private final CustomerCardRepository customerCardRepository;

    public CustomerCardServiceImpl(CustomerCardRepository customerCardRepository) {
        this.customerCardRepository = customerCardRepository;
    }

    @Override
    public CustomerCard save(CustomerCard customerCard) {
        log.debug("Request to save CustomerCard : {}", customerCard);
        return customerCardRepository.save(customerCard);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CustomerCard> findAll(Pageable pageable) {
        log.debug("Request to get all CustomerCards");
        return customerCardRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerCard> findOne(Long id) {
        log.debug("Request to get CustomerCard : {}", id);
        return customerCardRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CustomerCard : {}", id);
        customerCardRepository.deleteById(id);
    }


    @Override
	public String callStopingCard(String cardIdentifier, String reference, String reasonId, String note,
			String lang) throws ParserConfigurationException {

		// TODO Auto-generated method stub
		ArrayList<MediaType> acceptable = new ArrayList<MediaType>();

        //ignoring ssl certificatee
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

		acceptable.add(MediaType.APPLICATION_XML);
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl("https://192.168.41.45:8033/services/companioncard/StoppingCard_1_0")
				.queryParam("cardIdentifier", cardIdentifier)
				.queryParam("reference", reference)
				.queryParam("reasonId", reasonId)
				.queryParam("note", note)
				.queryParam("lang", lang);

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(acceptable);

		HttpEntity<?> request = new HttpEntity<>(headers);

		log.info("_______requete callStopingCard= "+request.toString());
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(),
				HttpMethod.GET, request, String.class);


        JSONObject json = XML.toJSONObject(response.getBody());
        log.info(json.toString());
		return json.toString();
	}

	@Override
	public String callUnStopingCard(String cardIdentifier, String reference, String note, String lang) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {

		// TODO Auto-generated method stub
				ArrayList<MediaType> acceptable = new ArrayList<MediaType>();

        //ignoring ssl certificatee
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

				acceptable.add(MediaType.APPLICATION_XML);
				UriComponentsBuilder builder = UriComponentsBuilder
						.fromHttpUrl("https://192.168.41.45:8033/services/companioncard/UnStoppingCard_1_0")
						.queryParam("cardIdentifier", cardIdentifier)
						.queryParam("reference", reference)
						.queryParam("note", note)
						.queryParam("lang", lang);


				HttpHeaders headers = new HttpHeaders();
				headers.setAccept(acceptable);

				HttpEntity<?> request = new HttpEntity<>(headers);

				log.info("_______requete callUnStopingCard = "+request.toString());
				RestTemplate restTemplate = getRestTemplate();
				ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(),
						HttpMethod.GET, request, String.class);

        JSONObject json = XML.toJSONObject(response.getBody());
        log.info(json.toString());
        return json.toString();
	}

    @Override
    public Page<CustomerCard> findAllByFilter(String keyword, Pageable pageable) {
        Page<CustomerCard> page;
        if (keyword != null && !keyword.isEmpty() && !keyword.equals("null") && !keyword.equals("undefined")) {
            page = customerCardRepository.findAllByFilter("%" + keyword + "%", pageable);
        } else {
            page = customerCardRepository.findAllOrder(pageable);
        }
        return page;
    }



    /******************************/
	@Override
	public ResponseEntity<String> callRetireCard(String cardIdentifier, String reference, String lang) {

		// TODO Auto-generated method stub
				ArrayList<MediaType> acceptable = new ArrayList<MediaType>();

        //ignoring ssl certificatee
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

				acceptable.add(MediaType.APPLICATION_XML);
				UriComponentsBuilder builder = UriComponentsBuilder
						.fromHttpUrl("https://192.168.41.165:9040/services/companioncard/RetireCard_1_0")

						.queryParam("cardIdentifier", cardIdentifier)
						.queryParam("reference", reference)
						.queryParam("lang", lang);

				HttpHeaders headers = new HttpHeaders();
				headers.setAccept(acceptable);


				HttpEntity<?> request = new HttpEntity<>(headers);

				log.info("_______requete CallRetireCard_1_0 = "+request.toString());
				RestTemplate restTemplate = new RestTemplate();
				ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(),
						HttpMethod.GET, request, String.class);

				return response;
	}


    public RestTemplate getRestTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        TrustStrategy acceptingTrustStrategy = (x509Certificates, s) -> true;
        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext, new NoopHostnameVerifier());
        CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(requestFactory);
        return restTemplate;
    }
}
