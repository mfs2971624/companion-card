package com.free.sn.service.impl;

import com.free.sn.domain.Transaction;
import com.free.sn.repository.TransactionRepository;
import com.free.sn.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link com.free.sn.domain.Transaction}.
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    private final TransactionRepository transactionRepository;

    public TransactionServiceImpl(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }


    @Override
    public Page<Transaction> findAllByFilter(Pageable pageable) {
       return transactionRepository.findAllByFilter(pageable);
    }

    @Override
    public List<Transaction> findAllByFilter() {
        return transactionRepository.findAllByFilter();
    }

    @Override
    public Optional<Transaction> findById(Long id) {
        return transactionRepository.findById(id);
    }




    @Override
    public Page<Transaction> search(String keyword, String transType, String status, Instant debut, Instant fin, Pageable pageable) {
        return transactionRepository.search(keyword, transType, status, debut, fin, pageable);
    }

    @Override
    public List<Transaction> search(String keyword, String transType, String status, Instant debut, Instant fin) {
        return transactionRepository.search(keyword, transType, status, debut, fin);
    }


}
