package com.free.sn.service;

import com.free.sn.client.ActivateCardClient;
import com.free.sn.client.ActivateCardResponse;
import com.free.sn.client.ActivateCardResponseParam;
import com.free.sn.web.rest.vm.ActivateCardVM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.bind.JAXBException;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class ActivateCardService {

    @Autowired
    ActivateCardClient client;

    public Map activateCard(ActivateCardVM activateCardVM) throws JAXBException {

        // Create Request Parameters
        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("msisdn", activateCardVM.getPhoneNumber());
        requestParams.put("pin", activateCardVM.getPinCode());
        requestParams.put("card_number", activateCardVM.getCardNumber());
        requestParams.put("lang", activateCardVM.getLang());

        // Call Activation Card Client Request
        ActivateCardResponse response = client.callActivateCardRequest(requestParams);

        System.out.println("response = " + response);

        // Convert Response to Map and return
        return convertResponseToMap(response);
    }

    private Map convertResponseToMap(ActivateCardResponse response) {

        // Construct Map for response
        Map<String, String> resultParams = new HashMap<>();

        for (ActivateCardResponseParam param : response.getParams()) {
            if (param.getName().equals("resultCode")) {
                resultParams.put("resultCode", param.getValue());
            }
            if (param.getName().equals("resultMessage")) {
                resultParams.put("resultMessage", param.getValue());
            }
            if (param.getName().equals("cardNumber")) {
                resultParams.put("cardNumber", param.getValue());
            }
            if (param.getName().equals("backendErrorCode")) {
                resultParams.put("backendErrorCode", param.getValue());
            }
            if (param.getName().equals("backendErrorMessage")) {
                resultParams.put("backendErrorMessage", param.getValue());
            }
            if (param.getName().equals("backendErrorType")) {
                resultParams.put("backendErrorType", param.getValue());
            }
        }

        return resultParams;
    }
}
