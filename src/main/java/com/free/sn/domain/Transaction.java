package com.free.sn.domain;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;

/**
 * A Transaction.
 */
@Entity
@Table(name = "TRANSACTIONS")
public class Transaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "TransactionId", length = 50)
    private String transactionId;

    @Size(max = 10)
    @Column(name = "TransactionType", length = 10)
    private String transactionType;

    @Column(name = "TransactionDate")
    private Instant transactionDate;

    @Column(name = "TransactionAmount")
    private Integer transactionAmount;

    @Column(name = "TransactionData")
    private String transactionData;

    @Size(max = 50)
    @Column(name = "Reference", length = 50)
    private String reference;

    @Size(max = 50)
    @Column(name = "ResponseStatusComviva", length = 50)
    private String responseStatusComviva;

    @Size(max = 255)
    @Column(name = "ResponseDetailsComviva", length = 255)
    private String responseDetailsComviva;

    @Size(max = 50)
    @Column(name = "ReferenceId", length = 50)
    private String referenceId;

    @Size(max = 50)
    @Column(name = "OperationName", length = 50)
    private String operationName;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Transaction transactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public Transaction transactionType(String transactionType) {
        this.transactionType = transactionType;
        return this;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Instant getTransactionDate() {
        return transactionDate;
    }

    public Transaction transactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public void setTransactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    public Transaction transactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
        return this;
    }

    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionData() {
        return transactionData;
    }

    public Transaction transactionData(String transactionData) {
        this.transactionData = transactionData;
        return this;
    }

    public void setTransactionData(String transactionData) {
        this.transactionData = transactionData;
    }

    public String getReference() {
        return reference;
    }

    public Transaction reference(String reference) {
        this.reference = reference;
        return this;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getResponseStatusComviva() {
        return responseStatusComviva;
    }

    public Transaction responseStatusComviva(String responseStatusComviva) {
        this.responseStatusComviva = responseStatusComviva;
        return this;
    }

    public void setResponseStatusComviva(String responseStatusComviva) {
        this.responseStatusComviva = responseStatusComviva;
    }

    public String getResponseDetailsComviva() {
        return responseDetailsComviva;
    }

    public Transaction responseDetailsComviva(String responseDetailsComviva) {
        this.responseDetailsComviva = responseDetailsComviva;
        return this;
    }

    public void setResponseDetailsComviva(String responseDetailsComviva) {
        this.responseDetailsComviva = responseDetailsComviva;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public Transaction referenceId(String referenceId) {
        this.referenceId = referenceId;
        return this;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getOperationName() {
        return operationName;
    }

    public Transaction operationName(String operationName) {
        this.operationName = operationName;
        return this;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Transaction)) {
            return false;
        }
        return id != null && id.equals(((Transaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Transaction{" +
            "id=" + getId() +
            ", transactionId='" + getTransactionId() + "'" +
            ", transactionType='" + getTransactionType() + "'" +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", transactionAmount=" + getTransactionAmount() +
            ", transactionData='" + getTransactionData() + "'" +
            ", reference='" + getReference() + "'" +
            ", responseStatusComviva='" + getResponseStatusComviva() + "'" +
            ", responseDetailsComviva='" + getResponseDetailsComviva() + "'" +
            ", referenceId='" + getReferenceId() + "'" +
            ", operationName='" + getOperationName() + "'" +
            "}";
    }
}
