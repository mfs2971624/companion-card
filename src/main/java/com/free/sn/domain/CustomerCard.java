package com.free.sn.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A CustomerCard.
 */
@Entity
@Table(name = "CUSTOMERS_CARDS")
public class CustomerCard implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "customer_msisdn")
    private String customerMsisdn;

    @Column(name = "customer_reference")
    private String customerReference;

    @Column(name = "customer_firstname")
    private String customerFirstName;

    @Column(name = "customer_lastname")
    private String customerLastName;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "trackingNumber")
    private String trackingNumber;

    @Column(name = "valid_date")
    private LocalDate validDate;

    @Column(name = "expiry_date")
    private Instant expiryDate;

    @Column(name = "card_type")
    private String cardType;

    @Column(name = "card_status")
    private String cardStatus;

    @Column(name = "creation_date")
    private Instant creationDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public CustomerCard customerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
        return this;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public CustomerCard customerReference(String customerReference) {
        this.customerReference = customerReference;
        return this;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public CustomerCard customerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
        return this;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public CustomerCard customerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
        return this;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public CustomerCard cardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDate getValidDate() {
        return validDate;
    }

    public CustomerCard validDate(LocalDate validDate) {
        this.validDate = validDate;
        return this;
    }

    public void setValidDate(LocalDate validDate) {
        this.validDate = validDate;
    }

    public Instant getExpiryDate() {
        return expiryDate;
    }

    public CustomerCard expiryDate(Instant expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }

    public void setExpiryDate(Instant expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCardType() {
        return cardType;
    }

    public CustomerCard cardType(String cardType) {
        this.cardType = cardType;
        return this;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public CustomerCard cardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
        return this;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public CustomerCard creationDate(Instant creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here


    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomerCard)) {
            return false;
        }
        return id != null && id.equals(((CustomerCard) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CustomerCard{" +
            "id=" + getId() +
            ", customerMsisdn='" + getCustomerMsisdn() + "'" +
            ", customerReference='" + getCustomerReference() + "'" +
            ", customerFirstName='" + getCustomerFirstName() + "'" +
            ", customerLastName='" + getCustomerLastName() + "'" +
            ", cardNumber='" + getCardNumber() + "'" +
            ", trackingNumber='" + getTrackingNumber() + "'" +
            ", validDate='" + getValidDate() + "'" +
            ", expiryDate='" + getExpiryDate() + "'" +
            ", cardType='" + getCardType() + "'" +
            ", cardStatus='" + getCardStatus() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            "}";
    }
}
