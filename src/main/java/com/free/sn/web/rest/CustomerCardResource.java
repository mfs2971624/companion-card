package com.free.sn.web.rest;

import com.free.sn.domain.CustomerCard;
import com.free.sn.service.CustomerCardService;
import com.free.sn.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.validation.Valid;
import javax.xml.bind.JAXBException;

/**
 * REST controller for managing {@link com.free.sn.domain.CustomerCard}.
 */
@RestController
@RequestMapping("/api")
public class CustomerCardResource {

    private final Logger log = LoggerFactory.getLogger(CustomerCardResource.class);

    private static final String ENTITY_NAME = "customerCard";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustomerCardService customerCardService;

    public CustomerCardResource(CustomerCardService customerCardService) {
        this.customerCardService = customerCardService;
    }

    /**
     * {@code POST  /customer-cards} : Create a new customerCard.
     *
     * @param customerCard the customerCard to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new customerCard, or with status {@code 400 (Bad Request)} if the customerCard has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/customer-cards")
    public ResponseEntity<CustomerCard> createCustomerCard(@RequestBody CustomerCard customerCard) throws URISyntaxException {
        log.debug("REST request to save CustomerCard : {}", customerCard);
        if (customerCard.getId() != null) {
            throw new BadRequestAlertException("A new customerCard cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerCard result = customerCardService.save(customerCard);
        return ResponseEntity.created(new URI("/api/customer-cards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /customer-cards} : Updates an existing customerCard.
     *
     * @param customerCard the customerCard to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated customerCard,
     * or with status {@code 400 (Bad Request)} if the customerCard is not valid,
     * or with status {@code 500 (Internal Server Error)} if the customerCard couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/customer-cards")
    public ResponseEntity<CustomerCard> updateCustomerCard(@RequestBody CustomerCard customerCard) throws URISyntaxException {
        log.debug("______________REST request to update CustomerCard : {}", customerCard);
        if (customerCard.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CustomerCard result = customerCardService.save(customerCard);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, customerCard.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /customer-cards} : get all the customerCards.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of customerCards in body.
     */
    /*@GetMapping("/customer-cards")
    public ResponseEntity<List<CustomerCard>> getAllCustomerCards(Pageable pageable) {
    	 log.info("@@@*_____dans la listes");
    	 log.debug("REST request to get a page of CustomerCards");
          Page<CustomerCard> page = customerCardService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }*/

    @GetMapping("/customer-cards")
    public ResponseEntity<List<CustomerCard>> getAllCustomerCards(@RequestParam(value = "keyword") String keyword, Pageable pageable) {
        log.debug("REST request to get a page of CustomerCards");
        log.info("keyword = {}", keyword);
        Page<CustomerCard> page = customerCardService.findAllByFilter(keyword, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /*
    @GetMapping("/transactions")
    public ResponseEntity<List<Transaction>> getAllTransactions(@RequestParam(value = "keyword") String keyword, Pageable pageable) {
        log.debug("REST request to get a page of Transactions");
        Page<Transaction> page = transactionService.findAllByFilter("%" + keyword + "%", pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
     */

    /**
     * {@code GET  /customer-cards/:id} : get the "id" customerCard.
     *
     * @param id the id of the customerCard to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the customerCard, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/customer-cards/{id}")
    public ResponseEntity<CustomerCard> getCustomerCard(@PathVariable Long id) {
        log.debug("REST request to get CustomerCard : {}", id);
        Optional<CustomerCard> customerCard = customerCardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(customerCard);
    }

    /**
     * {@code DELETE  /customer-cards/:id} : delete the "id" customerCard.
     *
     * @param id the id of the customerCard to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/customer-cards/{id}")
    public ResponseEntity<Void> deleteCustomerCard(@PathVariable Long id) {
        log.debug("REST request to delete CustomerCard : {}", id);
        customerCardService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }

    @PutMapping("/customer-cards/{id}")
	public ResponseEntity<CustomerCard> updateCustomer(@PathVariable(value = "id") Long id, @Valid @RequestBody CustomerCard customerDetails) throws Exception {
    	CustomerCard customer = customerCardService .findOne(id)
				.orElseThrow(() -> new Exception("customer introuvable pour cet id : " + id));

		log.info("_______@ dans updatecustomer  @_____________");
		customer.setCardStatus(customerDetails.getCardStatus());

		CustomerCard updatedcustomer = customerCardService.save(customer);
		log.info("@ dans updatedcustomer +-- after updating");

		return ResponseEntity.ok(updatedcustomer);
	}


	@GetMapping("stopping_card")
	@ResponseBody
    String stoppingCard(@RequestParam("cardIdentifier") String cardIdentifier,
                            @RequestParam("reference") String reference,
                            @RequestParam("reasonId") String reasonId,
                            @RequestParam("note") String note,
                            @RequestParam("lang") String lang) throws JAXBException {


		String response = "";

		try {



			 response = customerCardService.callStopingCard(cardIdentifier, reference, reasonId, note, lang);


			log.info("__________ stopping_card ___________ ");

			log.info("Réponse de stopping_card :  " + response);


		} catch (Exception e) {
			log.error("erreur  :  " + e.getMessage());
			e.printStackTrace();
		}
		log.info("Reponse de appel " + response);
		return response;
	}


	@GetMapping("unstopping_card")
	@ResponseBody
	String unstoppingCard(@RequestParam("cardIdentifier") String cardIdentifier,
						@RequestParam("reference") String reference,
						@RequestParam("note") String note,
						@RequestParam("lang") String lang) throws JAXBException {


		String response = "";

		try {



			 response = customerCardService.callUnStopingCard(cardIdentifier, reference, note, lang);


			log.info("*__________ Unstopping_card ___________* ");

			log.info("Réponse de Unstopping_card :  " + response);


		} catch (Exception e) {
			log.error("erreur  :  " + e.getMessage());
			e.printStackTrace();
		}
		log.info("Reponse de appel " + response);
		return response;
	}



	@GetMapping("retire_card")
	@ResponseBody
	String retireCard(@RequestParam("cardIdentifier") String cardIdentifier,
						@RequestParam("reference") String reference,
						@RequestParam("lang") String lang) throws JAXBException {


		ResponseEntity<String> response = null;

		try {


			 response = customerCardService.callRetireCard(cardIdentifier, reference, lang);


			log.info("*__________ Retire_Card ___________* ");

			log.info("Réponse de Retire_Card :  " + response.getBody());


		} catch (Exception e) {
			log.error("erreur  :  " + e.getMessage());
			e.printStackTrace();
		}
		//log.info("Reponse de appel " + response.getBody());
		return response.getBody();
	}
}
