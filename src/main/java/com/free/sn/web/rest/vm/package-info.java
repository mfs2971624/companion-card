/**
 * View Models used by Spring MVC REST controllers.
 */
package com.free.sn.web.rest.vm;
