package com.free.sn.web.rest;

import com.free.sn.domain.Transaction;
import com.free.sn.service.TransactionService;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.micrometer.core.instrument.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.free.sn.domain.Transaction}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class TransactionResource {

    private final Logger log = LoggerFactory.getLogger(TransactionResource.class);

    private final TransactionService transactionService;

    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * {@code GET  /transactions} : get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactions in body.
     */
    @GetMapping("/transactions")
    public ResponseEntity<List<Transaction>> getAllTransactions(Pageable pageable) {
        log.debug("REST request to get a page of Transactions");

        log.debug("REST request to get a page of Advances");
        Page<Transaction> page = transactionService.findAllByFilter(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());


    }

    /**
     * {@code GET  /transactions} : get all the transactions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactions in body.
     */
    @GetMapping("/transactions/search")
    public ResponseEntity<List<Transaction>> getAllTransactionsBySearch(@RequestParam (required = false) String keyword, @RequestParam (required = false) String transType,
                                                                        @RequestParam (required = false) String status, @RequestParam(value = "debut") String debut,
                                                                        @RequestParam(value = "fin") String fin, Pageable pageable) {

        Instant d1 = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant d2 = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        String st = !StringUtils.isBlank(status) ?  status : null ;
        String key = !StringUtils.isBlank(keyword) ? '%' + keyword + '%' : null ;
        String tt = !StringUtils.isBlank(transType) ? transType : null ;


        Page<Transaction> page = transactionService.search(key, tt, st, d1, d2, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());

    }

    @GetMapping("/transactions/report")
    public ResponseEntity<List<Transaction>> getAllReports() {


        List<Transaction> page = transactionService.findAllByFilter();
        return ResponseEntity.ok(page);

    }

    @GetMapping("/transactions/report/search")
    public ResponseEntity<List<Transaction>> getAllReportsBySearch(@RequestParam (required = false) String keyword, @RequestParam (required = false) String transType,
                                                                   @RequestParam (required = false) String status, @RequestParam(value = "debut") String debut,
                                                                   @RequestParam(value = "fin") String fin, Pageable pageable) {

        Instant d1 = !StringUtils.isBlank(debut) ? Instant.parse(debut) : null;
        Instant d2 = !StringUtils.isBlank(fin) ? Instant.parse(fin) : Instant.now();
        String st = !StringUtils.isBlank(status) ?  status : null ;
        String key = !StringUtils.isBlank(keyword) ? '%' + keyword + '%' : null ;
        String tt = !StringUtils.isBlank(transType) ? transType : null ;


        List<Transaction> page = transactionService.search(key, tt, st, d1, d2);
        return ResponseEntity.ok(page);

    }

    /**
     * {@code GET  /transactions/:id} : get the "id" transaction.
     *
     * @param id the id of the transaction to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transaction, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transactions/{id}")
    public ResponseEntity<Transaction> getTransaction(@PathVariable Long id) {
        log.debug("REST request to get Transaction : {}", id);
        Optional<Transaction> transaction = transactionService.findById(id);
        return ResponseUtil.wrapOrNotFound(transaction);
    }


}
