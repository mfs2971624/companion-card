package com.free.sn.web.rest.vm;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ActivateCardVM {

    @NotNull(message = "Le numéro de téléphone ne doit pas etre null")
    @NotEmpty(message = "Le numéro de téléphone ne doit pas etre vide")
    private String phoneNumber;

    @NotNull(message = "Le code pin ne doit pas etre null")
    @NotEmpty(message = "Le code pin ne doit pas etre vide")
    private String pinCode;

    @NotNull(message = "Le numéro de carte ne doit pas etre null")
    @NotEmpty(message = "Le numéro de carte  ne doit pas etre vide")
    private String cardNumber;

    @NotNull(message = "La langue ne doit pas etre null")
    @NotEmpty(message = "La langue  ne doit pas etre vide")
    private String lang;

    public ActivateCardVM() {
    }

    public ActivateCardVM(String phoneNumber, String pinCode, String cardNumber, String lang) {
        this.phoneNumber = phoneNumber;
        this.pinCode = pinCode;
        this.cardNumber = cardNumber;
        this.lang = lang;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public String toString() {
        return "ActivateCardVM{" +
            "phoneNumber='" + phoneNumber + '\'' +
            ", pinCode='" + pinCode + '\'' +
            ", cardNumber='" + cardNumber + '\'' +
            ", lang='" + lang + '\'' +
            '}';
    }
}
