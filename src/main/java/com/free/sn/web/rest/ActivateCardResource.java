package com.free.sn.web.rest;

import com.free.sn.service.ActivateCardService;
import com.free.sn.web.rest.errors.ActivateCardException;
import com.free.sn.web.rest.vm.ActivateCardVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.zalando.problem.Status;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Transactional
public class ActivateCardResource {

    private final Logger log = LoggerFactory.getLogger(ActivateCardResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActivateCardService service;

    public ActivateCardResource(ActivateCardService service) {
        this.service = service;
    }

    @PostMapping(value = "/activate-card", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> activateCard(@Valid @RequestBody ActivateCardVM activateCardVM) {
        log.debug("REST request to activate Card : {}", activateCardVM);

        try {
            // Call service for activation card
            Map<String, String> response = service.activateCard(activateCardVM);

            // Check Response resultCode
            int resultCode = Integer.parseInt(response.get("resultCode"));
            String resultMessage = response.get("resultMessage");
//            String cardNumber = response.get("cardNumber");
//            String backendErrorCode = response.get("backendErrorCode");
            String backendErrorMessage = response.get("backendErrorMessage");
            String backendErrorType = response.get("backendErrorType");

            // SUCCESS => 1, FAILURE => 0
            if (resultCode == 1) {
                return ResponseEntity.ok("La carte n° " + activateCardVM.getCardNumber() + " a été activée avec succès !");
            } else {
                throw new ActivateCardException("Echec d'activation de la carte n° " + activateCardVM.getCardNumber() + "\n." +
                    "Raison : " + backendErrorMessage, Status.BAD_REQUEST);
            }

        } catch (Exception e) {
          throw new ActivateCardException("Echec d'activation de la carte n° " + activateCardVM.getCardNumber() + "\n." +
                "Raison : " + e.getMessage(), Status.INTERNAL_SERVER_ERROR);
        }
    }
}
