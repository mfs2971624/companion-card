package com.free.sn.web.rest.errors;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ActivateCardException extends AbstractThrowableProblem {

    private static final long serialVersionUID = 1L;
    private String message;
    private Status status;

    public ActivateCardException() {
        super(ErrorConstants.ACTIVATE_CARD_FAILURE_TYPE, "Echec Activation Carte", Status.INTERNAL_SERVER_ERROR);
    }

    public ActivateCardException(String message, Status status) {
        super(ErrorConstants.ACTIVATE_CARD_FAILURE_TYPE, "Echec Activation Carte", status, message);
    }
}
