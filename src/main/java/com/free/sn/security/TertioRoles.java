package com.free.sn.security;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Getter

public enum TertioRoles {

    ADMIN_MFS("ROLE_ADMIN", "ROLE_ADMIN"),
    G_COMPANION_MFS_ADMINS("G_COMPANION_MFS_ADMINS", "ROLE_G_COMPANION_MFS_ADMINS"),
    G_COMPANION_MFS_READONLY("G_COMPANION_MFS_READONLY", "ROLE_G_COMPANION_MFS_READONLY"),
 //   USER("ROLE_USER", "ROLE_USER")
    ;

    public static List<TertioRoles> allRoles;

    static {
        List<TertioRoles> roles = new ArrayList<>(Arrays.asList(TertioRoles.values()));
        allRoles = Collections.unmodifiableList(roles);
    }

    private String activeDirectoryGroup;
    private String role;
    private GrantedAuthority authority;


    TertioRoles(String activeDirectoryGroup, String role) {
        this.activeDirectoryGroup = activeDirectoryGroup;
        this.role = role;
        this.authority = new SimpleGrantedAuthority(activeDirectoryGroup);
    }

}
