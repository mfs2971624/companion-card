package com.free.sn.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String COMPLAIN = "ROLE_COMPLAIN";

    public static final String B2B = "ROLE_B2B";

    public static final String AGENT = "ROLE_AGENT";

    public static final String CALL_CENTER = "ROLE_CALL_CENTER";

    public static final String USER = "ROLE_USER";

    public static final String MFS_ADMINS = "MFS_ADMINS";

    public static final String G_COMPANION_MFS_ADMINS = "ROLE_G_COMPANION_MFS_ADMINS";

    public static final String G_COMPANION_MFS_READONLY  = "ROLE_G_COMPANION_MFS_READONLY";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }

}
