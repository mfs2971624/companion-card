import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { CompanionCardTestModule } from '../../../test.module';
import { CustomerCardUpdateComponent } from 'app/entities/customer-card/customer-card-update.component';
import { CustomerCardService } from 'app/entities/customer-card/customer-card.service';
import { CustomerCard } from 'app/shared/model/customer-card.model';

describe('Component Tests', () => {
  describe('CustomerCard Management Update Component', () => {
    let comp: CustomerCardUpdateComponent;
    let fixture: ComponentFixture<CustomerCardUpdateComponent>;
    let service: CustomerCardService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CompanionCardTestModule],
        declarations: [CustomerCardUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CustomerCardUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CustomerCardUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CustomerCardService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CustomerCard(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CustomerCard();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
