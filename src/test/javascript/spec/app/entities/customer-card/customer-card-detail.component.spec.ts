import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CompanionCardTestModule } from '../../../test.module';
import { CustomerCardDetailComponent } from 'app/entities/customer-card/customer-card-detail.component';
import { CustomerCard } from 'app/shared/model/customer-card.model';

describe('Component Tests', () => {
  describe('CustomerCard Management Detail Component', () => {
    let comp: CustomerCardDetailComponent;
    let fixture: ComponentFixture<CustomerCardDetailComponent>;
    const route = ({ data: of({ customerCard: new CustomerCard(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CompanionCardTestModule],
        declarations: [CustomerCardDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CustomerCardDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CustomerCardDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load customerCard on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.customerCard).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
