import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { CustomerCardService } from 'app/entities/customer-card/customer-card.service';
import { ICustomerCard, CustomerCard } from 'app/shared/model/customer-card.model';

describe('Service Tests', () => {
  describe('CustomerCard Service', () => {
    let injector: TestBed;
    let service: CustomerCardService;
    let httpMock: HttpTestingController;
    let elemDefault: ICustomerCard;
    let expectedResult: ICustomerCard | ICustomerCard[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(CustomerCardService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new CustomerCard(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            validDate: currentDate.format(DATE_FORMAT),
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            creationDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a CustomerCard', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            validDate: currentDate.format(DATE_FORMAT),
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            creationDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            validDate: currentDate,
            expiryDate: currentDate,
            creationDate: currentDate,
          },
          returnedFromService
        );

        service.create(new CustomerCard()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a CustomerCard', () => {
        const returnedFromService = Object.assign(
          {
            customerMsisdn: 'BBBBBB',
            customerReference: 'BBBBBB',
            customerFirstName: 'BBBBBB',
            customerLastName: 'BBBBBB',
            cardNumber: 'BBBBBB',
            validDate: currentDate.format(DATE_FORMAT),
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            cardType: 'BBBBBB',
            cardStatus: 'BBBBBB',
            creationDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            validDate: currentDate,
            expiryDate: currentDate,
            creationDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of CustomerCard', () => {
        const returnedFromService = Object.assign(
          {
            customerMsisdn: 'BBBBBB',
            customerReference: 'BBBBBB',
            customerFirstName: 'BBBBBB',
            customerLastName: 'BBBBBB',
            cardNumber: 'BBBBBB',
            validDate: currentDate.format(DATE_FORMAT),
            expiryDate: currentDate.format(DATE_TIME_FORMAT),
            cardType: 'BBBBBB',
            cardStatus: 'BBBBBB',
            creationDate: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            validDate: currentDate,
            expiryDate: currentDate,
            creationDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a CustomerCard', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
