package com.free.sn.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.free.sn.web.rest.TestUtil;

public class CustomerCardTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerCard.class);
        CustomerCard customerCard1 = new CustomerCard();
        customerCard1.setId(1L);
        CustomerCard customerCard2 = new CustomerCard();
        customerCard2.setId(customerCard1.getId());
        assertThat(customerCard1).isEqualTo(customerCard2);
        customerCard2.setId(2L);
        assertThat(customerCard1).isNotEqualTo(customerCard2);
        customerCard1.setId(null);
        assertThat(customerCard1).isNotEqualTo(customerCard2);
    }
}
