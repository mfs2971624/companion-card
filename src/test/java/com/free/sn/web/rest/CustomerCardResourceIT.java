package com.free.sn.web.rest;

import com.free.sn.CompanionCardApp;
import com.free.sn.domain.CustomerCard;
import com.free.sn.repository.CustomerCardRepository;
import com.free.sn.service.CustomerCardService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustomerCardResource} REST controller.
 */
@SpringBootTest(classes = CompanionCardApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustomerCardResourceIT {

    private static final String DEFAULT_CUSTOMER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_REFERENCE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_REFERENCE = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CARD_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_CARD_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_VALID_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VALID_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_EXPIRY_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXPIRY_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CARD_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CARD_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_CARD_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_CARD_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private CustomerCardRepository customerCardRepository;

    @Autowired
    private CustomerCardService customerCardService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCustomerCardMockMvc;

    private CustomerCard customerCard;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerCard createEntity(EntityManager em) {
        CustomerCard customerCard = new CustomerCard()
            .customerMsisdn(DEFAULT_CUSTOMER_MSISDN)
            .customerReference(DEFAULT_CUSTOMER_REFERENCE)
            .customerFirstName(DEFAULT_CUSTOMER_FIRST_NAME)
            .customerLastName(DEFAULT_CUSTOMER_LAST_NAME)
            .cardNumber(DEFAULT_CARD_NUMBER)
            .validDate(DEFAULT_VALID_DATE)
            .expiryDate(DEFAULT_EXPIRY_DATE)
            .cardType(DEFAULT_CARD_TYPE)
            .cardStatus(DEFAULT_CARD_STATUS)
            .creationDate(DEFAULT_CREATION_DATE);
        return customerCard;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerCard createUpdatedEntity(EntityManager em) {
        CustomerCard customerCard = new CustomerCard()
            .customerMsisdn(UPDATED_CUSTOMER_MSISDN)
            .customerReference(UPDATED_CUSTOMER_REFERENCE)
            .customerFirstName(UPDATED_CUSTOMER_FIRST_NAME)
            .customerLastName(UPDATED_CUSTOMER_LAST_NAME)
            .cardNumber(UPDATED_CARD_NUMBER)
            .validDate(UPDATED_VALID_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .cardType(UPDATED_CARD_TYPE)
            .cardStatus(UPDATED_CARD_STATUS)
            .creationDate(UPDATED_CREATION_DATE);
        return customerCard;
    }

    @BeforeEach
    public void initTest() {
        customerCard = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomerCard() throws Exception {
        int databaseSizeBeforeCreate = customerCardRepository.findAll().size();
        // Create the CustomerCard
        restCustomerCardMockMvc.perform(post("/api/customer-cards")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customerCard)))
            .andExpect(status().isCreated());

        // Validate the CustomerCard in the database
        List<CustomerCard> customerCardList = customerCardRepository.findAll();
        assertThat(customerCardList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerCard testCustomerCard = customerCardList.get(customerCardList.size() - 1);
        assertThat(testCustomerCard.getCustomerMsisdn()).isEqualTo(DEFAULT_CUSTOMER_MSISDN);
        assertThat(testCustomerCard.getCustomerReference()).isEqualTo(DEFAULT_CUSTOMER_REFERENCE);
        assertThat(testCustomerCard.getCustomerFirstName()).isEqualTo(DEFAULT_CUSTOMER_FIRST_NAME);
        assertThat(testCustomerCard.getCustomerLastName()).isEqualTo(DEFAULT_CUSTOMER_LAST_NAME);
        assertThat(testCustomerCard.getCardNumber()).isEqualTo(DEFAULT_CARD_NUMBER);
        assertThat(testCustomerCard.getValidDate()).isEqualTo(DEFAULT_VALID_DATE);
        assertThat(testCustomerCard.getExpiryDate()).isEqualTo(DEFAULT_EXPIRY_DATE);
        assertThat(testCustomerCard.getCardType()).isEqualTo(DEFAULT_CARD_TYPE);
        assertThat(testCustomerCard.getCardStatus()).isEqualTo(DEFAULT_CARD_STATUS);
        assertThat(testCustomerCard.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
    }

    @Test
    @Transactional
    public void createCustomerCardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerCardRepository.findAll().size();

        // Create the CustomerCard with an existing ID
        customerCard.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerCardMockMvc.perform(post("/api/customer-cards")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customerCard)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerCard in the database
        List<CustomerCard> customerCardList = customerCardRepository.findAll();
        assertThat(customerCardList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCustomerCards() throws Exception {
        // Initialize the database
        customerCardRepository.saveAndFlush(customerCard);

        // Get all the customerCardList
        restCustomerCardMockMvc.perform(get("/api/customer-cards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerCard.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerMsisdn").value(hasItem(DEFAULT_CUSTOMER_MSISDN)))
            .andExpect(jsonPath("$.[*].customerReference").value(hasItem(DEFAULT_CUSTOMER_REFERENCE)))
            .andExpect(jsonPath("$.[*].customerFirstName").value(hasItem(DEFAULT_CUSTOMER_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].customerLastName").value(hasItem(DEFAULT_CUSTOMER_LAST_NAME)))
            .andExpect(jsonPath("$.[*].cardNumber").value(hasItem(DEFAULT_CARD_NUMBER)))
            .andExpect(jsonPath("$.[*].validDate").value(hasItem(DEFAULT_VALID_DATE.toString())))
            .andExpect(jsonPath("$.[*].expiryDate").value(hasItem(DEFAULT_EXPIRY_DATE.toString())))
            .andExpect(jsonPath("$.[*].cardType").value(hasItem(DEFAULT_CARD_TYPE)))
            .andExpect(jsonPath("$.[*].cardStatus").value(hasItem(DEFAULT_CARD_STATUS)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getCustomerCard() throws Exception {
        // Initialize the database
        customerCardRepository.saveAndFlush(customerCard);

        // Get the customerCard
        restCustomerCardMockMvc.perform(get("/api/customer-cards/{id}", customerCard.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(customerCard.getId().intValue()))
            .andExpect(jsonPath("$.customerMsisdn").value(DEFAULT_CUSTOMER_MSISDN))
            .andExpect(jsonPath("$.customerReference").value(DEFAULT_CUSTOMER_REFERENCE))
            .andExpect(jsonPath("$.customerFirstName").value(DEFAULT_CUSTOMER_FIRST_NAME))
            .andExpect(jsonPath("$.customerLastName").value(DEFAULT_CUSTOMER_LAST_NAME))
            .andExpect(jsonPath("$.cardNumber").value(DEFAULT_CARD_NUMBER))
            .andExpect(jsonPath("$.validDate").value(DEFAULT_VALID_DATE.toString()))
            .andExpect(jsonPath("$.expiryDate").value(DEFAULT_EXPIRY_DATE.toString()))
            .andExpect(jsonPath("$.cardType").value(DEFAULT_CARD_TYPE))
            .andExpect(jsonPath("$.cardStatus").value(DEFAULT_CARD_STATUS))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingCustomerCard() throws Exception {
        // Get the customerCard
        restCustomerCardMockMvc.perform(get("/api/customer-cards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomerCard() throws Exception {
        // Initialize the database
        customerCardService.save(customerCard);

        int databaseSizeBeforeUpdate = customerCardRepository.findAll().size();

        // Update the customerCard
        CustomerCard updatedCustomerCard = customerCardRepository.findById(customerCard.getId()).get();
        // Disconnect from session so that the updates on updatedCustomerCard are not directly saved in db
        em.detach(updatedCustomerCard);
        updatedCustomerCard
            .customerMsisdn(UPDATED_CUSTOMER_MSISDN)
            .customerReference(UPDATED_CUSTOMER_REFERENCE)
            .customerFirstName(UPDATED_CUSTOMER_FIRST_NAME)
            .customerLastName(UPDATED_CUSTOMER_LAST_NAME)
            .cardNumber(UPDATED_CARD_NUMBER)
            .validDate(UPDATED_VALID_DATE)
            .expiryDate(UPDATED_EXPIRY_DATE)
            .cardType(UPDATED_CARD_TYPE)
            .cardStatus(UPDATED_CARD_STATUS)
            .creationDate(UPDATED_CREATION_DATE);

        restCustomerCardMockMvc.perform(put("/api/customer-cards")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCustomerCard)))
            .andExpect(status().isOk());

        // Validate the CustomerCard in the database
        List<CustomerCard> customerCardList = customerCardRepository.findAll();
        assertThat(customerCardList).hasSize(databaseSizeBeforeUpdate);
        CustomerCard testCustomerCard = customerCardList.get(customerCardList.size() - 1);
        assertThat(testCustomerCard.getCustomerMsisdn()).isEqualTo(UPDATED_CUSTOMER_MSISDN);
        assertThat(testCustomerCard.getCustomerReference()).isEqualTo(UPDATED_CUSTOMER_REFERENCE);
        assertThat(testCustomerCard.getCustomerFirstName()).isEqualTo(UPDATED_CUSTOMER_FIRST_NAME);
        assertThat(testCustomerCard.getCustomerLastName()).isEqualTo(UPDATED_CUSTOMER_LAST_NAME);
        assertThat(testCustomerCard.getCardNumber()).isEqualTo(UPDATED_CARD_NUMBER);
        assertThat(testCustomerCard.getValidDate()).isEqualTo(UPDATED_VALID_DATE);
        assertThat(testCustomerCard.getExpiryDate()).isEqualTo(UPDATED_EXPIRY_DATE);
        assertThat(testCustomerCard.getCardType()).isEqualTo(UPDATED_CARD_TYPE);
        assertThat(testCustomerCard.getCardStatus()).isEqualTo(UPDATED_CARD_STATUS);
        assertThat(testCustomerCard.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingCustomerCard() throws Exception {
        int databaseSizeBeforeUpdate = customerCardRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustomerCardMockMvc.perform(put("/api/customer-cards")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(customerCard)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerCard in the database
        List<CustomerCard> customerCardList = customerCardRepository.findAll();
        assertThat(customerCardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCustomerCard() throws Exception {
        // Initialize the database
        customerCardService.save(customerCard);

        int databaseSizeBeforeDelete = customerCardRepository.findAll().size();

        // Delete the customerCard
        restCustomerCardMockMvc.perform(delete("/api/customer-cards/{id}", customerCard.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustomerCard> customerCardList = customerCardRepository.findAll();
        assertThat(customerCardList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
